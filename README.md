# Penguin Slide

Penguin slide is a speedrun platformer game written in typescript.

# How To Run

### to run local version of Penguin Slide (after cloning from git):

-   Install node and npm (if you haven't already) `npm install`.
-   If you want to build it once, use `npm run build`.
-   To run the tests, use `npm run test`.
-   If you want it to build every time a change is made, use `npm run start`.
