// variables ending in '_g' are measured in game coordinates.

import { Penguin } from "./penguin";
import { Camera } from "./camera";
import {
    width_g2px,
    height_g2px,
    x_g2px,
    y_g2px,
    ScreenSize,
    numberOfPhysicsFrames,
    musicSpeed,
} from "./utils";
import { Block } from "./block";
const fps = 60;
const gravity = 0.03;
// sets up various objects and displays. Ran when page is first loaded (window.onload).

function setup(
    penguin: Penguin,
    camera: Camera,
    screenSize: ScreenSize,
    blocks: Block[]
) {
    const penimg = addImg("penguin", penguin, screenSize, camera);
    window.onkeydown = function (key) {
        keypress(key.keyCode, penguin);
    };
    const music = document.createElement("audio");
    music.autoplay = true;
    music.loop = true;
    music.controls = true;
    music.src = "music/Level_1-1_Icy_Beginings.mp3";
    music.volume = 0.5;
    music.id = "music";
    music.playbackRate = 1; //penguin.vx_g;
    document.body.appendChild(music);
    document.body.style.backgroundImage = "url('img/background.png')";
    document.body.style.backgroundRepeat = "repeat";
    document.body.style.height = "100vh";
    document.body.style.backgroundPosition = "100px 100px";
    window.requestAnimationFrame((t: DOMHighResTimeStamp) => {
        animate(penguin, camera, screenSize, penimg, blocks, t, null);
    });
}

interface Sized {
    width_g: number;
    height_g: number;
}

function addImg(
    name: string,
    obj: Sized,
    screenSize: ScreenSize,
    camera: Camera
) {
    const img = document.createElement("img");
    img.src = `img/${name}.svg`;
    img.style.width = width_g2px(obj.width_g, screenSize, camera) + "px";
    img.style.height = height_g2px(obj.height_g, screenSize, camera) + "px";
    img.style.position = "absolute";
    document.body.appendChild(img);
    return img;
}

// calculates physics every frame (called by update).
function runPhysics(penguin: Penguin, camera: Camera) {
    if (penguin.grounded === false) {
        penguin.vy_g += gravity;
        penguin.y_g += penguin.vy_g;
    } else {
        penguin.vx_g += 0.005;
        penguin.vy_g = 0;
    }
    penguin.x_g += penguin.vx_g;
    camera.x_g = penguin.x_g - 60;
    camera.y_g = penguin.y_g - 45;
    const music = document.getElementById("music") as HTMLAudioElement;
    music.playbackRate = musicSpeed(penguin.vx_g);
}

// registers key inputs for game controls. Called by update.
function keypress(key: number, penguin: Penguin) {
    if (penguin.grounded === true) {
        penguin.grounded = false;
        penguin.y_g -= 1;
        penguin.vy_g -= 1;
    }
}

/**
 * Runs a single frame update
 */
function update(
    penguin: Penguin,
    camera: Camera,
    screenSize: ScreenSize,
    penimg: HTMLElement,
    blocks: Block[],
    frames: number
) {
    document.body.style.backgroundPosition =
        width_g2px(camera.x_g, screenSize, camera) / -2 +
        "px " +
        height_g2px(camera.y_g, screenSize, camera) / -2 +
        "px";
    const penback_g = penguin.x_g - penguin.width_g/2
    const penbottom_g = penguin.y_g + penguin.height_g/2
    const penfront_g = penguin.x_g + penguin.width_g/2
    const pentop_g = penguin.y_g - penguin.height_g/2
    for (const block of blocks) {
        const blockback_g = block.x_g - block.width_g/2
        const blockbottom_g = block.y_g + block.height_g/2
        const blockfront_g = block.x_g + block.width_g/2
        const blocktop_g = block.y_g - block.height_g/2
        if (penfront_g > blockback_g && penback_g < blockfront_g) {
            if (penbottom_g > blocktop_g && pentop_g < blockbottom_g) {
                penguin.grounded = true;
                penguin.y_g = blocktop_g - penguin.height_g/2;
            }
        }
    }
    for (let f = 0; f < frames; f++) {
        runPhysics(penguin, camera);
    }

    penimg.style.left = x_g2px(penguin.x_g, screenSize, camera) + "px";
    penimg.style.top = y_g2px(penguin.y_g, screenSize, camera) + "px";
    for (const block of blocks) {
        block.update(camera, screenSize);
    }
}

/**
 * Called every frame (window.requestAnimationFrame). Calculates missed frames
 * between animation frames.
 */
function animate(
    penguin: Penguin,
    camera: Camera,
    screenSize: ScreenSize,
    penimg: HTMLElement,
    blocks: Block[],
    t: DOMHighResTimeStamp,
    t_last: DOMHighResTimeStamp | null
) {
    const [frames, msLeftOver] = numberOfPhysicsFrames(fps, t, t_last);

    update(penguin, camera, screenSize, penimg, blocks, frames);

    window.requestAnimationFrame((t_next: DOMHighResTimeStamp) => {
        animate(
            penguin,
            camera,
            screenSize,
            penimg,
            blocks,
            t_next,
            t - msLeftOver
        );
    });
}

window.onload = () => {
    const camera = new Camera();
    const screenSize = new ScreenSize(
        document.documentElement.clientWidth,
        document.documentElement.clientHeight
    );
    setup(new Penguin(), camera, screenSize, [
        new Block(
            0,
            60,
            40,
            8,
            document.createElement("img"),
            camera,
            screenSize
        ),
        new Block(
            80,
            40,
            55,
            10,
            document.createElement("img"),
            camera,
            screenSize
        ),
    ]);
};
