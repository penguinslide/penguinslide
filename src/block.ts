import { Camera } from "./camera";
import { width_g2px, height_g2px, x_g2px, y_g2px, ScreenSize } from "./utils";

export class Block {
    //  x and y values are based in the centre of the block.
    constructor(
        public x_g: number,
        public y_g: number,
        public width_g: number,
        public height_g: number,
        public img: HTMLElement,
        camera: Camera,
        screenSize: ScreenSize
    ) {
        img.style.backgroundImage = "url('img/blockice.svg')";
        img.style.backgroundRepeat = "repeat";
        img.style.width = width_g2px(width_g, screenSize, camera) + "px";
        img.style.height = height_g2px(height_g, screenSize, camera) + "px";
        img.style.position = "absolute";
        document.body.appendChild(img);
    }
    update(camera: Camera, screenSize: ScreenSize) {
        this.img.style.left = x_g2px(this.x_g, screenSize, camera) + "px";
        this.img.style.top = y_g2px(this.y_g, screenSize, camera) + "px";
    }
}
