import { Camera } from "./camera";

export class ScreenSize {
    width_px: number;
    height_px: number;

    constructor(width_px: number, height_px: number) {
        this.width_px = width_px;
        this.height_px = height_px;
    }
}

export function width_g2px(
    width_g: number,
    screenSize: ScreenSize,
    camera: Camera
) {
    return (screenSize.width_px / camera.width_g) * width_g;
}

export function height_g2px(
    height_g: number,
    screenSize: ScreenSize,
    camera: Camera
) {
    return (screenSize.height_px / camera.height_g) * height_g;
}
export function x_g2px(x_g: number, screenSize: ScreenSize, camera: Camera) {
    return (screenSize.width_px / camera.width_g) * (x_g - camera.x_g);
}

export function y_g2px(y_g: number, screenSize: ScreenSize, camera: Camera) {
    return (screenSize.height_px / camera.height_g) * (y_g - camera.y_g);
}

export function numberOfPhysicsFrames(
    fps: number,
    t: DOMHighResTimeStamp,
    t_last: DOMHighResTimeStamp | null
): [number, number] {
    if (t_last === null) {
        return [0, 0];
    }
    const msPerFrame = 1000 / fps;
    const frames = (t - t_last) / msPerFrame;
    const roundedframes = Math.floor(frames);
    const msLeftOver = (frames - roundedframes) * msPerFrame;

    return [roundedframes, msLeftOver];
}
export function musicSpeed(pspeed_g: number) {
    if (pspeed_g >= 3) {
        return 2.5;
    }
    if (pspeed_g <= 0) {
        return 0.5;
    }
    return (2 / 3) * pspeed_g + 1 / 2;
}
