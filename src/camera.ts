export class Camera {
    //  x and y values are based in the top-left of the Camera.
    x_g: number = 0;
    y_g: number = 0;
    vx_g: number = 0;
    vy_g: number = 0;
    width_g: number = 160;
    height_g: number = 90;
}
