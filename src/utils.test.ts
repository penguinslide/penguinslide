import {
    width_g2px,
    height_g2px,
    x_g2px,
    y_g2px,
    ScreenSize,
    numberOfPhysicsFrames,
    musicSpeed,
} from "./utils";
import { Camera } from "./camera";

describe("scaling", () => {
    test("width of 160 game units is 1920 on an HD screen", () => {
        const screenSize = new ScreenSize(1920, 1080);
        expect(width_g2px(160, screenSize, new Camera())).toBe(1920);
    });

    test("width of 80 game units is 320 on an 640x480 screen", () => {
        const screenSize = new ScreenSize(640, 480);
        expect(width_g2px(80, screenSize, new Camera())).toBe(320);
    });
    test("height of 90 game units is 1080 on an HD screen", () => {
        const screenSize = new ScreenSize(1920, 1080);
        expect(height_g2px(90, screenSize, new Camera())).toBe(1080);
    });
    test("camera is 0, penguin is 20, real position on 200x200 screen is 25", () => {
        const screenSize = new ScreenSize(200, 200);
        expect(x_g2px(20, screenSize, new Camera())).toBe(25);
    });
    test("camera is 20, penguin is 20, real x position on 200x200 screen is 0", () => {
        const screenSize = new ScreenSize(200, 200);
        const camera = new Camera();
        camera.x_g = 20;
        expect(x_g2px(20, screenSize, camera)).toBe(0);
    });
    test("camera is 0, penguin is 45, real y position on 200x200 screen is 100", () => {
        const screenSize = new ScreenSize(200, 200);
        const camera = new Camera();
        camera.y_g = 0;
        expect(y_g2px(45, screenSize, camera)).toBe(100);
    });
});

describe("timings", () => {
    test("50fps, 20ms passed means 1 frame exactly", () => {
        expect(numberOfPhysicsFrames(50, 20, 0)).toStrictEqual([1, 0]);
    });

    test("10fps, 250ms passed means 2 frames passed and 50ms left over", () => {
        expect(numberOfPhysicsFrames(10, 1250, 1000)).toStrictEqual([2, 50]);
    });
});
describe("music", () => {
    test("If penguin speed is 0, then music speed is 0.5", () => {
        expect(musicSpeed(0)).toBe(0.5);
    });
    test("If penguin speed is 3 then music speed is 2.5", () => {
        expect(musicSpeed(3)).toBe(2.5);
    });
    test("If penguin speed is 6 then music speed is 2.5", () => {
        expect(musicSpeed(6)).toBe(2.5);
    });
});
