export class Penguin {
    //  x and y values are based in the centre of the Penguin.
    x_g: number = 20;
    y_g: number = 45;
    //  velocity is measured in game units per second.
    vx_g: number = 0;
    vy_g: number = 0;
    width_g: number = 21;
    height_g: number = 8;
    grounded: boolean = false;
    jump: boolean = false;
}
